+++
author = "Khoa Nguyen"
categories = ["2fa-usecase"]
date = 2020-01-12T05:00:00Z
description = "How to Turn On 2FA for Twitter using PrivaKeeper"
image = "/images/twitter_feature.png"
title = "How to Turn On 2FA for Twitter using PrivaKeeper"
type = "post"

+++
**Step 1**. Go to [Twitter Account Settings](https://twitter.com/settings/account)

**Step 2**. Select "Security" option

![](/images/security.png)

**Step 3**. Select "Two-factor authentication"

![](/images/twitter_step2.png)

**Step 4.** Then select "Authentication app". It will appear a popup. Click on "Start" button.

![](/images/twitter_addictional_pic.png)

**Step 5**. Verify with your current password.

**Step 6**. You should click on **"Can't scan QR code"** to get the secret code

![](/images/twitter_step4.png)

**Step 7**. Get your secret code

![](/images/twitter_step5.png)

**Step 8**. Go to the [PrivaKeeper](https://privakeeper.com/) app, select **Private 2FAs**. You should click on add new button.

![](/images/twitter_step6.png)

Type your secret code that you get from step above.

With Tittle, Account, Url and Folder, you can input any rememberable text.

**Step 9**. Copy the 6-digit verification code generated by the PrivaKeeper app.

![](/images/twitter_step7.png)

**Step 10**. Go back to Twitter's popup, click on Next button

![](/images/twitter_step8.png)

**Step 11**. Paste the code you get from PrivaKeeper app, and press Verify

![](/images/twitter_step9.png)

Besides, you can try other Dapps:

  
PrivaKeeper - PRIVATE STORAGE THAT RESPECTS YOUR PRIVACY

[https://privakeeper.com/](https://privakeeper.com/ "https://privakeeper.com/")  
BlockResume - A modern resume generator which respects your privacy

[https://blockresume.me/](https://blockresume.me/ "https://blockresume.me/")

Calorie - PRIVATE CALORIES COUNTER WEB APP

[https://calorie.fit/landing](https://calorie.fit/landing "https://calorie.fit/landing")  
Finisher - PRIVATE ACTIVITY TRACKER APP

[https://finisher.pro/landing](https://finisher.pro/landing "https://finisher.pro/landing")  
Book Billionaire

[https://bookbillionaire.com/](https://bookbillionaire.com/ "https://bookbillionaire.com/")  
Great Quotes - Great Quotes of all time

[https://greatquotes.life/](https://greatquotes.life/ "https://greatquotes.life/")  
Embolden It - A decentralized way to highlight words on website.

[https://emboldenit.com/](https://emboldenit.com/ "https://emboldenit.com/")